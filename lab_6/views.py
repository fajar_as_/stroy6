from django.shortcuts import render, redirect
from .forms import Status
from .models import Kabar
from django.views.decorators.http import require_POST

# Create your views here
def index(request):
    list_status = Kabar.objects.order_by('-waktu')
    form = Status()
    context = {'list_status': list_status, 'form': form}
    return render(request, 'landing.html', context)

@require_POST
def tambahStatus(request):
    form = Status(request.POST)

    if form.is_valid():
        new_status = Kabar(status = request.POST['status'])
        new_status.save()
    
    return redirect ('homepage:index')