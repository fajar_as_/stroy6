from django import forms

class Status(forms.Form):
    status = forms.CharField(label='Status',required= True, max_length=300,widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Enter your Status'}))
